/** Map **********/
var map = (function () {
    var Map = {
        domEl: document.getElementById('map'),
        el: null,
        itinerary: [],
        directionsDisplay: null,
        directionService: null,
        infos: [],
        center: {lat: 45.8978388, lng: 6.1262386},
        userMarker: null,
        oldUserMarker: null,
        speed: null,

        /** Init map **********/
        init: function () {
            console.log('Init map');
            this.getUrlInfos();
            this.create();
            this.directionsDisplay = new google.maps.DirectionsRenderer();
            this.directionsService = new google.maps.DirectionsService();
        },

        /** Get user position constantly **********/
        userPosition: function (pos) {
            this.oldUserMarker = this.userMarker;
            this.userMarker !== null ? this.userMarker.setMap(null) : '';
            this.center = {
                lat: pos.coords.latitude,
                lng: pos.coords.longitude
            };
            this.el.setCenter(this.center);

            this.speed = pos.coords.speed;

            this.userMarker = new google.maps.Marker({
                position: this.center,
                map: this.el,
                icon: 'img/user_marker.png',
                title: "Vous êtes ici"
            });
            this.userMarker.setMap(this.el);
            console.log('User Position');
            this.nearSteps();
        },

        /** Display errors bind to GPS **********/
        errorPosition: function (error) {
            var info = "Erreur lors de la géolocalisation : ";
            switch (error.code) {
                case error.TIMEOUT:
                    info += "Timeout !";
                    break;
                case error.PERMISSION_DENIED:
                    info += "Vous n’avez pas donné la permission";
                    break;
                case error.POSITION_UNAVAILABLE:
                    info += "La position n’a pu être déterminée";
                    break;
                case error.UNKNOWN_ERROR:
                    info += "Erreur inconnue";
                    break;
            }
            alert(info);
            TweenMax.set(document, 5, {
                onComplete: navigator.geolocation.watchPosition(this.userPosition.bind(this), this.errorPosition.bind(this)),
                onCompleteScope: this
            });
        },

        /** Check distance between User and Steps **********/
        nearSteps: function () {
            var distance;
            var stepDisplay = new google.maps.InfoWindow;
            var i = 0;

            for (i; i < this.itinerary.length; i++) {
                distance = findDistance(
                    this.itinerary[i].getPosition().lat(),
                    this.itinerary[i].getPosition().lng(),
                    this.userMarker.getPosition().lat(),
                    this.userMarker.getPosition().lng()
                );
                if (distance <= 0.01 && this.speed < 2) {
                    this.setText(stepDisplay, this.itinerary[i]);
                } else if (this.speed >= 2) {
                    alert('Vous marchez drôlement vite quand même.');
                }
            }
        },

        /** Create map **********/
        create: function () {
            console.log('Create map');

            this.el = new google.maps.Map(this.domEl, {
                center: this.center,
                scrollwheel: false,
                zoom: 17,
                mapTypeControl: false,
                styles: [{
                    "featureType": "administrative",
                    "elementType": "geometry",
                    "stylers": [{"visibility": "off"}]
                }, {
                    "featureType": "administrative.province",
                    "elementType": "labels",
                    "stylers": [{"visibility": "off"}, {"color": "#ffffff"}]
                }, {
                    "featureType": "administrative.locality",
                    "elementType": "labels",
                    "stylers": [{"visibility": "simplified"}, {"color": "#ffffff"}]
                }, {
                    "featureType": "administrative.neighborhood",
                    "elementType": "labels",
                    "stylers": [{"visibility": "simplified"}, {"color": "#ffffff"}]
                }, {
                    "featureType": "landscape.man_made",
                    "elementType": "geometry.fill",
                    "stylers": [{"visibility": "on"}, {"color": "#252525"}]
                }, {
                    "featureType": "landscape.man_made",
                    "elementType": "geometry.stroke",
                    "stylers": [{"visibility": "on"}, {"color": "#303030"}, {"weight": "1"}]
                }, {
                    "featureType": "landscape.man_made",
                    "elementType": "labels",
                    "stylers": [{"visibility": "simplified"}]
                }, {
                    "featureType": "landscape.natural",
                    "elementType": "geometry.fill",
                    "stylers": [{"visibility": "on"}, {"color": "#252525"}]
                }, {
                    "featureType": "poi",
                    "elementType": "geometry.fill",
                    "stylers": [{"visibility": "on"}, {"color": "#252525"}]
                }, {
                    "featureType": "poi",
                    "elementType": "geometry.stroke",
                    "stylers": [{"visibility": "on"}, {"color": "#666666"}]
                }, {
                    "featureType": "poi",
                    "elementType": "labels.text",
                    "stylers": [{"visibility": "simplified"}]
                }, {
                    "featureType": "poi",
                    "elementType": "labels.icon",
                    "stylers": [{"visibility": "off"}]
                }, {
                    "featureType": "road",
                    "elementType": "geometry",
                    "stylers": [{"lightness": 100}, {"visibility": "simplified"}, {"color": "#111111"}]
                }, {
                    "featureType": "road",
                    "elementType": "labels",
                    "stylers": [{"visibility": "simplified"}, {"color": "#666666"}]
                }, {
                    "featureType": "road",
                    "elementType": "labels.icon",
                    "stylers": [{"visibility": "off"}]
                }, {
                    "featureType": "transit",
                    "elementType": "geometry",
                    "stylers": [{"color": "#000000"}]
                }, {
                    "featureType": "transit",
                    "elementType": "labels",
                    "stylers": [{"visibility": "on"}]
                }, {
                    "featureType": "transit",
                    "elementType": "labels.text",
                    "stylers": [{"visibility": "simplified"}]
                }, {
                    "featureType": "transit",
                    "elementType": "labels.icon",
                    "stylers": [{"visibility": "simplified"}]
                }, {
                    "featureType": "transit.line",
                    "elementType": "geometry",
                    "stylers": [{"visibility": "off"}, {"lightness": 700}]
                }, {
                    "featureType": "transit.station.rail",
                    "elementType": "labels",
                    "stylers": [{"visibility": "off"}]
                }, {"featureType": "water", "elementType": "all", "stylers": [{"color": "#111111"}]}]
            });

            if (navigator.geolocation)
                navigator.geolocation.watchPosition(this.userPosition.bind(this), this.errorPosition.bind(this));

            this.listen();
        },

        /** Add event listener **********/
        listen: function () {
            google.maps.event.addListener(this.el, 'click', function (e) {
                Map.createItinerary(e.latLng);
            });
            $('#reset-marker').on('click', this.resetMarkers.bind(this));
        },

        /** Create an itinerary **********/
        createItinerary: function (location) {
            var reset = $('#reset-marker');
            if (this.itinerary.length < 2) {
                /** Create a marker **********/
                var marker = new google.maps.Marker({
                    position: location,
                    map: this.el
                });

                this.itinerary.push(marker);
                if (this.itinerary.length == 2) {
                    this.route(false);
                }
                TweenMax.to(reset, .2, {x: 0});
            }
            this.nearSteps();
        },

        /** Clear markers on the map **********/
        resetMarkers: function () {
            var reset = $('#reset-marker');
            this.route(true);
            var i = 0;
            for (i; i < this.itinerary.length; i++) {
                /** Remove all markers on the map **********/
                this.itinerary[i].setMap(null);
            }
            /** Remove all marker from the array of marker **********/
            this.itinerary = [];
            TweenMax.to(reset, .2, {x: -100});
        },

        /** Draw a route **********/
        route: function (clear) {
            if (clear) {
                this.directionsDisplay.setMap(null);
            } else {
                this.directionsDisplay.setMap(this.el);

                var start = new google.maps.LatLng(this.itinerary[0].position.lat(), this.itinerary[0].position.lng());
                var end = new google.maps.LatLng(this.itinerary[1].position.lat(), this.itinerary[1].position.lng());
                var stepDisplay = new google.maps.InfoWindow;
                var request = {
                    origin: start,
                    destination: end,
                    travelMode: google.maps.TravelMode.WALKING
                };
                this.directionsService.route(request, function (result, status) {
                    if (status == google.maps.DirectionsStatus.OK) {
                        this.directionsDisplay.setDirections(result);
                        this.displaySteps(result, stepDisplay);
                    } else {
                        alert("Couldn't get directions:" + status);
                    }
                }.bind(this));
            }
        },

        /** Display steps on the route **********/
        displaySteps: function (directionResult, stepDisplay) {
            var myRoute = directionResult.routes[0].legs[0];
            var i = 0;
            for (i; i < myRoute.steps.length; i++) {
                var marker = this.itinerary[i] = this.itinerary[i] || new google.maps.Marker;
                marker.setMap(this.el);
                marker.setPosition(myRoute.steps[i].start_location);

                marker.num = Math.round(Math.random() * this.infos.length);
                this.itinerary[i] = marker;
                // console.log(stepDisplay);
                // this.setText(stepDisplay, marker);
            }
        },

        /** Add information to markers **********/
        setText: function (stepDisplay, marker) {
            google.maps.event.addListener(marker, 'click', function () {
                stepDisplay.setContent(this.infos[marker.num]);
                stepDisplay.open(this.el, marker);
            }.bind(this));
        },

        /** Get RSS urls **********/
        getUrlInfos: function () {
            var datas = {};
            var jsonUrl = 'datas/datas.json';

            $.ajax({
                url: jsonUrl,
                dataType: 'json',
                success: (function (res) {
                    this.getInfos(res);
                }.bind(this)),
                error: function (e) {
                    console.log('JSON file error : ' + e);
                    alert('Actualités non disponibles.');
                }
            }, this);
        },

        /** Get infos from the RSS urls **********/
        getInfos: function (obj) {
            var datas = JSON.parse(localStorage.getItem('datas'));
            this.infos = [];

            if (datas !== null && typeof datas.themes !== 'undefined') {
                var i = 0;
                for (i; i < datas.themes.length; i++) {
                    $.ajax({
                        url: 'https://api.rss2json.com/v1/api.json?rss_url=' + encodeURIComponent(obj.themes[i].link),
                        dataType: 'jsonp',
                        success: (function (data) {
                            this.formatDatas(data.items);
                        }.bind(this)),
                        error: function () {
                            alert('Error getting news');
                        }
                    }, this);
                }
            }

        },

        /** Format datas for display **********/
        formatDatas: function (items) {
            var html;
            var i = 0;
            for (i; i < items.length; i++) {
                html = '<div class="infowindow">';
                html += '<h1 class="ft_special">' + items[i].title + '</h1>';
                html += '<p>' + items[i].content + '</p>';
                html += '</div>';
                this.infos.push(html);
            }
            this.shuffle();
        },

        /** Shuffle the array of news **********/
        shuffle: function () {
            this.infos.sort(function () {
                return 0.5 - Math.random()
            });
        }
    };

    return Map;
})();

/** Menu **********/
var menu = (function () {
    var Menu = {
        el: $('#menu'),

        /** Init menu **********/
        init: function () {
            console.log('Init menu');
            UserPage.init();
            this.listen();
        },

        /** Add event listeners **********/
        listen: function () {
            this.el.find('#hamburger').on('click', this.toggle.bind(this, $('#menu-page')));
            this.el.find('#user').on('click', this.toggle.bind(this, $('#user-page')));
        },

        /** Open menu **********/
        open: function (el) {
            var tl = new TimelineMax();

            tl.set(el, {display: "block"});

            el.attr('id') == 'menu-page' ? tl.to(el, .3, {bottom: $(window).height() - 100}) : tl.to(el, .3, {bottom: -1});
            tl.to(el.find('.block-part'), 0, {opacity: 0, y: 10});
            tl.staggerTo(el.find('.block-part'), .2, {opacity: 1, y: 0}, .2);
            el.addClass('open');
        },

        /** Toggle menu **********/
        toggle: function (el) {
            var tl = new TimelineMax();
            var winH = $(window).height();
            var menu = $('#menu-page');
            var user = $('#user-page');

            tl.to(menu.find('.block-part'), 0, {opacity: 0});
            tl.to(menu, .3, {bottom: winH - 50});
            tl.set(menu, {display: "none"});
            tl.to(user.find('.block-part'), 0, {opacity: 0});
            tl.to(user, .3, {bottom: winH - 50});
            tl.set(user, {
                display: "none", onComplete: function () {
                    el.attr('id') == 'menu-page' ? user.removeClass('open') : menu.removeClass('open');
                    !el.hasClass('open') ? this.open(el) : el.removeClass('open');
                }.bind(this)
            });
        }
    };

    var UserPage = {
        el: $('#user-page'),

        /** Init User page **********/
        init: function () {
            console.log('Init UserPage');
            this.getUrlInfos();
            this.listen();
        },

        /** Add event listener **********/
        listen: function () {
            this.el.find('button').on('click', this.saveDatas.bind(this));
        },

        /** Get RSS urls **********/
        getUrlInfos: function () {
            var datas = {};
            var jsonUrl = 'datas/datas.json';

            $.ajax({
                url: jsonUrl,
                dataType: 'json',
                success: (function (res) {
                    this.getDatas(res);
                }.bind(this)),
                error: function (e) {
                    console.log('JSON file error : ' + e);
                    alert('Actualités non disponibles.');
                }
            }, this);
        },

        /** Get available themes **********/
        getDatas: function (obj) {
            var datas, choice, input, label, cName;
            var i = 0;
            for (i; i < Object.keys(obj.themes).length; i++) {
                cName = obj.themes[i].name;

                choice = document.createElement('div');
                choice.classList.add('block-semi');

                input = document.createElement('input');
                input.setAttribute('name', 'themes');
                input.setAttribute('type', 'checkbox');
                input.setAttribute('value', cName);
                input.setAttribute('id', 'c_' + cName);

                label = document.createElement('label');
                label.setAttribute('for', 'c_' + cName);
                label.innerHTML = cName;

                choice.appendChild(input);
                choice.appendChild(label);

                $('.checkbox-groups').append(choice);
            }

            if (localStorage.length !== 0) {
                datas = localStorage.getItem('datas')
                this.displayDatas(JSON.parse(datas));
            } else if ($('body').attr('id') === 'carte') {
                Menu.toggle($('#user-page'));
            }
        },

        /** Display available themes **********/
        displayDatas: function (datas) {
            if (typeof datas.name !== 'undefined') {
                this.el.find('#name').val(datas.name);
                this.el.find('h2 span').html(datas.name);
            }
            typeof datas.age !== 'undefined' ? this.el.find('#age').val(datas.age) : '';

            if (typeof datas.themes !== 'undefined') {
                var i = 0;
                for (i; i < datas.themes.length; i++) {
                    this.el.find('#c_' + datas.themes[i]).attr('checked', true);
                }
            }
        },

        /** Save datas in localStorage **********/
        saveDatas: function () {
            var datas = this.el.find('#user-form').serialize().split('&');

            var datasObj = {};
            var i = 0;
            for (i; i < datas.length; i++) {
                var split = datas[i].split('=');

                if (!(split[0] in datasObj)) {
                    datasObj[split[0]] = [];
                }
                datasObj[split[0]].push(split[1]);
            }

            localStorage.clear();
            localStorage.setItem('datas', JSON.stringify(datasObj));
            map.getUrlInfos();
            Menu.toggle(this.el);
        }
    };

    return Menu;
})(map);

/** Home **********/
var home = (function () {
    var Home = {
        el: null,

        /** Init home page **********/
        init: function () {
            console.log('On homepage');
            this.el = $('#home');
            this.animation();
        },

        /** Animation **********/
        animation: function () {
            var tl = new TimelineMax({
                onComplete: function () {
                    tl2.play();
                }
            });
            var logo = this.el.find('.logo');
            logo.html(logo.text().replace(/(\w)/g, "<span>$&</span>"));

            tl.staggerTo(logo.find('span'), .3, {y: 0, opacity: 1}, .1, "+=1");
            tl.to(this.el.find('.slogan'), .2, {scale: 1}, "+=.5");
            tl.staggerTo(this.el.find('.tutorial p'), .6, {opacity: 1, y:0}, 2, "+=1");
            tl.to(this.el.find('.btn'), .3, {scale: 1}, "+=1.5");

            var tl2 = new TimelineMax({
                repeat: -1
            });
            tl2.staggerTo(logo.find('span'), .2, {y: -20}, .1, "+=3");
            tl2.staggerTo(logo.find('span'), .2, {y: 0}, .1, "-=1");
            tl2.pause();
        }
    };

    return Home;
})();

/** App **********/
var app = (function () {
    var App = {
        /** Init app **********/
        init: function () {
            console.log('Init app');
            this.launch();
        },

        /** Launch app **********/
        launch: function () {
            var page = $('body').attr('id');

            page === 'home' ? home.init() : menu.init();
        }
    };

    return App;
})(menu, home);

app.init();

/** Launch App **********/
function initMap() {
    map.init();
}


/***************************
 * Functions
 **************************/

/** Find distance between two coords **********/
function findDistance(t1, n1, t2, n2) {
    var Rk = 6373; // mean radius of the earth (km) at 39 degrees from the equator
    var lat1, lon1, lat2, lon2, dlat, dlon, a, c, dk, km;

    // convert coordinates to radians
    lat1 = deg2rad(t1);
    lon1 = deg2rad(n1);
    lat2 = deg2rad(t2);
    lon2 = deg2rad(n2);

    // find the differences between the coordinates
    dlat = lat2 - lat1;
    dlon = lon2 - lon1;

    // here's the heavy lifting
    a = Math.pow(Math.sin(dlat / 2), 2) + Math.cos(lat1) * Math.cos(lat2) * Math.pow(Math.sin(dlon / 2), 2);
    c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a)); // great circle distance in radians
    dk = c * Rk; // great circle distance in km

    // round the results down to the nearest 1/1000
    km = round(dk);

    return km;
}


// convert degrees to radians
function deg2rad(deg) {
    rad = deg * Math.PI / 180; // radians = degrees * pi/180
    return rad;
}

// round to the nearest 1/1000
function round(x) {
    return Math.round(x * 1000) / 1000;
}

/***************************
 * Ideas
 **************************/
// var tl = new TimelineMax();
// var el;
// var elC = $('body');
// var i = 0;
// var winH = $(window).height();
// for (i; i < 50; i++) {
//     el = document.createElement('div');
//     el.classList.add('icon-footprints');
//     // el.style.top = ($(window).height() + 50) + 'px';
//     el.style.left = Math.round(Math.random() * $(window).width()) + 'px';
//     el.style.transform = 'scale(' + (Math.random() + .5) + ')';
//     elC.append(el);
// }
// el = $('.icon-footprints');
// tl.staggerTo(el, 0, {y: winH + 50}, 0);
// tl.staggerTo(el, Math.random() * 3 + 7, {y: -50, z: 0.01, repeat: -1}, .2);
// tl.repeat(-1);


// var tl = new TimelineMax();
// var el = $('#launch');
// var h1 = el.find('h1');
// var h2 = el.find('h2');
// var winH = $(window).height();
//
// h1.html(h1.text().replace(/(\w)/g, "<span>$&</span>"));
// h2.html(h2.text().replace(/(\w)/g, "<span>$&</span>"));
//
// tl.to(h1.find('span'), 0, {y: -100, opacity: 0});
// tl.to(h2.find('span'), 0, {scale: 0});
// tl.to(el.find('h3'), 0, {opacity: 0, y: 10});
//
// tl.staggerTo(h1.find('span'), 1, {y: 0, opacity: 1, ease: Bounce.easeOut}, .3, "+=1");
// tl.staggerTo(h2.find('span'), 1, {scale: 2, ease: Bounce.easeOut}, .1, "+=.5");
// tl.staggerTo(h2.find('span'), 1, {scale: 1, ease: Bounce.easeOut}, .1, "-=1.2");
// tl.to(el.find('h3'), .2, {opacity: 1, y: 0}, "+=1");
//
// tl.to(el, 1, {y: -winH}, "+=2");